# swoole-cli静态运行工具(**仅自己方便阅读用**)
> swoole_cli原下载地址：[https://github.com/swoole/swoole-src](https://github.com/swoole/swoole-src "swoole官方")
> 
> 编译文件地址：[https://github.com/crazywhalecc/static-php-cli](https://github.com/crazywhalecc/static-php-cli "编译库")
> 
## 开始使用
+ windows(仅支持64位)
    1. git clone git@gitee.com:vanve/php-static-tools.git 或者下载下来 解压 
    2. 把windows64/bin目录添加至我得电脑-系统设置-环境变量中
    3. 终端中运行 swoole-cli 查看是否有输出信息
    


+ linux
    1. git clone git@gitee.com:vanve/php-static-tools.git 或者下载下来 解压 
    2. 直接运行linux64/bin中得swoole_cli文件即可 如果需要配置全局的话请看下文
    3. 也可以添加至环境变量
        + 所有用户
         ```
        vi /etc/profile
        export PATH=/路径
        source /etc/profile
        ```
        + 当前用户
       ```
       vi /root/.bashrc
       export PATH=/路径
       source /etc/profile
        ```
         
    4. 不想配置环境变量 简单粗暴的话 直接把linux64/bin目录中所有的执行文件包括composer扔着/usr/local/bin 
        + ps：如果需要用scomposer（下文再说）的话 把composer文件扔至/usr目录下


+ mac(mac需要自己去自己得环境编译)


## scomposer  


### 使用方式和composer一致 这个是为了配合swoole-cli的环境  

例如  

 ```
      somposer require xxxx:xxx
      scomposer update -o
      scomposer install
      ...
      ...
      ...
 ```


## 运行项目  
### 以hyperf为例  
 ```
      swoole-cli -c php.ini ./bin/hyperf.php start
 ```
### **注意**  
+ windows的话 在test目录中还加了ini和start.bat 放入hyperf文件直接运行start.bat即可
+ 热重启的话hyperf框架自带的Watcher有点问题 可以自行查找热更新插件或者应用


## 再次申明此文件仅共自己参阅使用 如有侵犯请联系我删除

